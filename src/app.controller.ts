import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    console.log('AMBIENTE');
    console.log(process.env.NODE_ENV);
    console.log(process.env.PORT);
    return this.appService.getHello();
  }
}
