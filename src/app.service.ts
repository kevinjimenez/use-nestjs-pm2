import { Inject, Injectable } from '@nestjs/common';
import config from './environment/config';
import { ConfigType } from '@nestjs/config';

@Injectable()
export class AppService {
  constructor(
    @Inject(config.KEY)
    private readonly _configService: ConfigType<typeof config>,
  ) {}

  getHello(): string {
    console.log(this._configService);
    return 'Hello World!';
  }
}
