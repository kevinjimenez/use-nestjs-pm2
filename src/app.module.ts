import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { enviroments } from './environment/environment';
import config from './environment/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      // envFilePath: enviroments[process.env.NODE_ENV],
      load: [config],
      isGlobal: true,
    }),
    // DatabaseModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
