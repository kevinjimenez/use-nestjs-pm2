import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    database: {
      sqlServer: {
        host: process.env.DATABASE_HOST,
        port: +process.env.DATABASE_PORT,
        database: process.env.DATABASE_NAME,
        schema: process.env.DATABASE_NAME_SCHEMA,
        synchronize: JSON.parse(process.env.DATABASE_SYNCHRONIZE),
        dropSchema: JSON.parse(process.env.DATABASE_DROPSCHEMA),
      },
    },
    bus: {
      hostBus: process.env.BUS_HOST,
      portBus: +process.env.BUS_PORT,
      segmentoBus: process.env.BUS_SEGEMENTO,
    },
    biometrico: {
      hostBiometrico: process.env.BOMETRICO_HOST,
      portBiometrico: process.env.BOMETRICO_PORT,
    },
    jwt: {
      secret: process.env.JWT_SECRET,
    },
    port: process.env.PORT,
    haha: process.env.HAHA,
  };
});
